export const prefixTemplate = (connectTarget, userName) => {
  return `<span class="connectTarget">${connectTarget}</span><span class="userName">${userName}</span>`;
};
